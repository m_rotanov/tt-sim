# Requirments:

* Virtualbox
* Vagrant
* Ansible

# Run:

`vagrant up`

# Test requests:

```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"username": "user","email": "user@host.domain","password_hash": "123456"}' \
  http://127.0.0.1:8080/api/user
```

```
curl http://127.0.0.1:8080
curl http://127.0.0.1:8080/hello.html
curl http://127.0.0.1:8080/api/user
```