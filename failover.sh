#!/bin/bash

#Example of how NOT to do failover!!!

timer="0"
while true; do

  ping -c 1 192.168.241.201 &> /dev/null
  let "timer=$timer+$?"
  if [ $timer -gt 10 ]; then
    echo "FAILOVER HAPPEND"
    touch /tmp/postgresql-trigger
    ip address add 192.168.241.201/24 dev enp0s8
    exit 0
  fi

done